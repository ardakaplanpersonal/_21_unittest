package com.androidegitim.unittest;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

//    @Test
//    public void addition_isCorrect() throws Exception {
//        assertEquals(4, 2 + 2);
//    }


    @Test
    public void addTest() throws Exception {

        int result = MainActivity.add(2, 2);


        assertEquals(4, result);
    }

    @Test
    public void addTest2() throws Exception {

        int result = MainActivity.add(2, 2);


        assertEquals(4, result);
    }

    @Test
    public void addTest3() throws Exception {

        int result = MainActivity.add(2, 2);


        assertEquals(6, result);
    }

    @Test
    public void addTest4() throws Exception {

        int result = MainActivity.add(2, 2);


        assertEquals(4, result);
    }
}